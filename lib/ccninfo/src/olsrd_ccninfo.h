
/*
 * The olsr.org Optimized Link-State Routing daemon(olsrd)
 * Copyright (c) 2004, Andreas Tonnesen(andreto@olsr.org)
 *                     includes code by Bruno Randolf
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of olsr.org, olsrd nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Visit http://www.olsr.org for more information.
 *
 * If you find this software useful feel free to make a donation
 * to the project. For more information see the website or contact
 * the copyright holders.
 *
 */

/*
 * Dynamic linked library for the olsr.org olsr daemon
 */

#ifndef _OLSRD_CCNINFO
#define _OLSRD_CCNINFO

#include "olsr_types.h"
#include "olsrd_plugin.h"
#include "plugin_util.h"
#include "parser.h"

/* uncomment this to allow connections from 127.0.0.1 regardless of olsrd.conf (useful to allow externel ip/network + localhost) (ipv4 only)*/
/* #define CCNINFO_ALLOW_LOCALHOST */

#define CCN_PLUGIN_VERSION 0x01
#define CCN_MESSAGE_TYPE 136

#define CCN_TLV_TYPE_URI 230
#define CCN_TLV_TYPE_CCNB 231
#define CCN_TLV_DEFAULT_FLAGS 0x10 /* plain type+flags+length+value */

#define MAX_TLV_PAYLOAD 65350
#define MAX_NAME_LEN 65300
#define CCN_PLUGIN_BUFFER_MAX_SIZE 65536

#define CCN_DEFAULT_EMISSION_INTERVAL 5
#define CCN_DEFAULT_VTIME 200

/* regular expression that matches valid CCN names */
#define CCN_VALID_NAMES_REGEX "^[a-zA-Z0-9\\.\\_\\-\\/\\%\\~]\\{1,256\\}$"

extern union olsr_ip_addr ccninfo_accept_ip;
extern union olsr_ip_addr ccninfo_listen_ip;
extern int ipc_port;
extern int nompr;

struct ccn_tlv {
  uint8_t type;
  uint8_t flags;
  uint8_t length;
  uint8_t value[];
} __attribute__((packed));

struct ccn_message {
  uint8_t olsr_msgtype; /* CCN_MESSAGE_TYPE */
  uint8_t olsr_vtime;
  uint16_t olsr_msgsize;
  uint32_t originator;
  uint8_t ttl;
  uint8_t hopcnt;
  uint16_t seqno;
  uint8_t version; /* CCN_PLUGIN_VERSION */
  uint8_t tlv_count; /* How many prefixes are contained in the message */
  uint16_t reserved;
  uint8_t tlvs [MAX_TLV_PAYLOAD];
} __attribute__((packed));

int olsrd_plugin_interface_version(void);
int olsrd_plugin_init(void);
void olsr_plugin_exit(void);
void olsrd_get_plugin_parameters(const struct olsrd_plugin_parameters **params, int *size);
int ccninfo_init(void);
bool ccn_parser(union olsr_message *m, struct interface *in_if __attribute__ ((unused)), union olsr_ip_addr *ipaddr);
void ccn_generator (void *foo __attribute__((unused)));

struct name_entry {
		struct name_entry *next;
		char *name;
		olsr_reltime vtime;
		struct timer_entry *name_timer;
		struct name_entry_head *head;
};

struct name_entry_head {
		struct name_entry *next;
};

struct ccn_entry {
		uint32_t originator;
		struct name_entry_head names;
		struct ccn_entry *next;
};

struct ccn_entry_head {
		struct ccn_entry *next;
};


void free_ccn_entries(void);
void free_name_entries(struct name_entry_head *name_head);
bool names_parser(uint8_t *tlvs, unsigned int tlv_count, uint32_t originator, olsr_reltime vtime);
bool append_name_entry(char *name, struct name_entry_head *nhead, olsr_reltime vtime);
void print_all_ccn_entries(int debuglevel);
int ccn_register (char *req);
bool ccn_name_register(char *name, bool unregister);
struct name_entry *get_name_entry(char *name, struct name_entry_head *nhead);
void free_name_entry(struct name_entry *nentry);
bool delete_name_entry(char *name, struct name_entry_head *nhead);
int get_our_names(uint8_t *tlvs, int *length, int maxlength);
struct ccn_entry * get_ccn_entry_by_originator(uint32_t originator);
bool ccn_name_is_valid(char *name);
bool add_ccn_name(uint32_t originator, char *name, olsr_reltime vtime);

#endif

/*
 * Local Variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 */
