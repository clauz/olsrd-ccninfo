/*
 * The olsr.org Optimized Link-State Routing daemon(olsrd)
 * Content Centric Networking plug-in
 * based on the txtinfo plug-in
 *
 * Copyright (c) 2012, Claudio Pisa <claudio.pisa@uniroma2.it>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of olsr.org, olsrd nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Visit http://www.olsr.org for more information.
 *
 * If you find this software useful feel free to make a donation
 * to the project. For more information see the website or contact
 * the copyright holders.
 *
 */

/*
 * Dynamic linked library for the olsr.org olsr daemon
 */


#include <sys/types.h>
#include <sys/socket.h>
#if !defined WIN32
#include <sys/select.h>
#endif
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <regex.h>

#include "ipcalc.h"
#include "olsr.h"
#include "olsr_types.h"
#include "net_olsr.h"
#include "lq_plugin.h"
#include "common/autobuf.h"
#include "gateway.h"

#include "olsrd_ccninfo.h"
#include "olsrd_plugin.h"

#ifdef WIN32
#define close(x) closesocket(x)
#endif

static int ipc_socket;

/* IPC initialization function */
static int plugin_ipc_init(void);

static void send_info(unsigned int /*send_what*/, int /*socket*/);

static void ipc_action(int, void *, unsigned int);

#define TXT_IPC_BUFSIZE 256

#define SIW_CCN 0x0800
#define SIW_CCN_REG_OK 0x1000
#define SIW_CCN_REG_KO 0x2000
#define SIW_CCN_UNREG_OK 0x4000
#define SIW_CCN_UNREG_KO 0x8000

#define MAX_CLIENTS 3

static char *outbuffer[MAX_CLIENTS];
static size_t outbuffer_size[MAX_CLIENTS];
static size_t outbuffer_written[MAX_CLIENTS];
static int outbuffer_socket[MAX_CLIENTS];
static int outbuffer_count;
static int ccn_emission_interval = CCN_DEFAULT_EMISSION_INTERVAL;
static int ccn_validity_time = CCN_DEFAULT_VTIME;
regex_t regx;

struct name_entry *current_our_name;

static struct timer_entry *writetimer_entry;
struct timer_entry *msg_gen_timer = NULL;
struct ccn_entry_head *ccn_all_entries = NULL;

static int
set_plugin_ccn_name(const char *value, void *data __attribute__ ((unused)), set_plugin_parameter_addon addon __attribute__ ((unused)))
{
		char tmpname[MAX_NAME_LEN];

		strncpy((char *)&tmpname, value, MAX_NAME_LEN);

		if(!ccn_name_is_valid((char *)&tmpname))
				return 0;

		if(add_ccn_name(olsr_cnf->main_addr.v4.s_addr, tmpname, 0))
				return 0;

		return 1;
}


static const struct olsrd_plugin_parameters plugin_parameters[] = {
  {.name = "port",.set_plugin_parameter = &set_plugin_port,.data = &ipc_port},
  {.name = "accept",.set_plugin_parameter = &set_plugin_ipaddress,.data = &ccninfo_accept_ip},
  {.name = "listen",.set_plugin_parameter = &set_plugin_ipaddress,.data = &ccninfo_listen_ip},
  {.name = "name",.set_plugin_parameter = &set_plugin_ccn_name, .data = NULL},
  {.name = "interval", .set_plugin_parameter = &set_plugin_int, .data = &ccn_emission_interval},
  {.name = "vtime", .set_plugin_parameter = &set_plugin_int, .data = &ccn_validity_time},
};

/**
 *Do initialization here
 *
 *This function is called by the my_init
 *function in uolsrd_plugin.c
 */
int
olsrd_plugin_init(void)
{
  /* Initial IPC value */
  ipc_socket = -1;
  plugin_ipc_init();

  return 1;
}

/**
 * destructor - called at unload
 */
void
olsr_plugin_exit(void)
{
  if (ipc_socket != -1)
    close(ipc_socket);
  regfree(&regx);
  free_ccn_entries();
  if(ccn_all_entries != NULL)
		  free(ccn_all_entries);
}

void
olsrd_get_plugin_parameters(const struct olsrd_plugin_parameters **params, int *size)
{
  *params = plugin_parameters;
  *size = sizeof(plugin_parameters) / sizeof(*plugin_parameters);
}

static int
plugin_ipc_init(void)
{
  union olsr_sockaddr sst;
  uint32_t yes = 1;
  socklen_t addrlen;

  /* Init ipc socket */
  if ((ipc_socket = socket(olsr_cnf->ip_version, SOCK_STREAM, 0)) == -1) {
#ifndef NODEBUG
    olsr_printf(1, "(CCNINFO) socket()=%s\n", strerror(errno));
#endif
    return 0;
  } else {
    if (setsockopt(ipc_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)) < 0) {
#ifndef NODEBUG
      olsr_printf(1, "(CCNINFO) setsockopt()=%s\n", strerror(errno));
#endif
      return 0;
    }
#if (defined __FreeBSD__ || defined __FreeBSD_kernel__) && defined SO_NOSIGPIPE
    if (setsockopt(ipc_socket, SOL_SOCKET, SO_NOSIGPIPE, (char *)&yes, sizeof(yes)) < 0) {
      perror("SO_REUSEADDR failed");
      return 0;
    }
#endif
    /* Bind the socket */

    /* complete the socket structure */
    memset(&sst, 0, sizeof(sst));
    if (olsr_cnf->ip_version == AF_INET) {
      sst.in4.sin_family = AF_INET;
      addrlen = sizeof(struct sockaddr_in);
#ifdef SIN6_LEN
      sst.in4.sin_len = addrlen;
#endif
      sst.in4.sin_addr.s_addr = ccninfo_listen_ip.v4.s_addr;
      sst.in4.sin_port = htons(ipc_port);
    } else {
      sst.in6.sin6_family = AF_INET6;
      addrlen = sizeof(struct sockaddr_in6);
#ifdef SIN6_LEN
      sst.in6.sin6_len = addrlen;
#endif
      sst.in6.sin6_addr = ccninfo_listen_ip.v6;
      sst.in6.sin6_port = htons(ipc_port);
    }

    /* bind the socket to the port number */
    if (bind(ipc_socket, &sst.in, addrlen) == -1) {
#ifndef NODEBUG
      olsr_printf(1, "(CCNINFO) bind()=%s\n", strerror(errno));
#endif
      return 0;
    }

    /* show that we are willing to listen */
    if (listen(ipc_socket, 1) == -1) {
#ifndef NODEBUG
      olsr_printf(1, "(CCNINFO) listen()=%s\n", strerror(errno));
#endif
      return 0;
    }

    /* Register with olsrd */
    add_olsr_socket(ipc_socket, &ipc_action, NULL, NULL, SP_PR_READ);

#ifndef NODEBUG
    olsr_printf(2, "(CCNINFO) listening on port %d\n", ipc_port);
#endif
  }
  return 1;
}

static void
ipc_action(int fd, void *data __attribute__ ((unused)), unsigned int flags __attribute__ ((unused)))
{
  union olsr_sockaddr pin;

  char addr[INET6_ADDRSTRLEN];
  fd_set rfds;
  struct timeval tv;
  unsigned int send_what = 0;
  int ipc_connection;

  socklen_t addrlen = sizeof(pin);

  if ((ipc_connection = accept(fd, &pin.in, &addrlen)) == -1) {
#ifndef NODEBUG
    olsr_printf(1, "(CCNINFO) accept()=%s\n", strerror(errno));
#endif
    return;
  }

  tv.tv_sec = tv.tv_usec = 0;
  if (olsr_cnf->ip_version == AF_INET) {
    if (inet_ntop(olsr_cnf->ip_version, &pin.in4.sin_addr, addr, INET6_ADDRSTRLEN) == NULL)
      addr[0] = '\0';
    if (!ip4equal(&pin.in4.sin_addr, &ccninfo_accept_ip.v4) && ccninfo_accept_ip.v4.s_addr != INADDR_ANY) {
#ifdef CCNINFO_ALLOW_LOCALHOST
      if (pin.in4.sin_addr.s_addr != INADDR_LOOPBACK) {
#endif
        olsr_printf(1, "(CCNINFO) From host(%s) not allowed!\n", addr);
        close(ipc_connection);
        return;
#ifdef CCNINFO_ALLOW_LOCALHOST
      }
#endif
    }
  } else {
    if (inet_ntop(olsr_cnf->ip_version, &pin.in6.sin6_addr, addr, INET6_ADDRSTRLEN) == NULL)
      addr[0] = '\0';
    /* Use in6addr_any (::) in olsr.conf to allow anybody. */
    if (!ip6equal(&in6addr_any, &ccninfo_accept_ip.v6) && !ip6equal(&pin.in6.sin6_addr, &ccninfo_accept_ip.v6)) {
      olsr_printf(1, "(CCNINFO) From host(%s) not allowed!\n", addr);
      close(ipc_connection);
      return;
    }
  }

#ifndef NODEBUG
  olsr_printf(2, "(CCNINFO) Connect from %s\n", addr);
#endif

  /* purge read buffer to prevent blocking on linux */
  FD_ZERO(&rfds);
  FD_SET((unsigned int)ipc_connection, &rfds);  /* Win32 needs the cast here */
  if (0 <= select(ipc_connection + 1, &rfds, NULL, NULL, &tv)) {
    char requ[CCN_PLUGIN_BUFFER_MAX_SIZE];
    ssize_t s = recv(ipc_connection, (void *)&requ, sizeof(requ), 0);   /* Win32 needs the cast here */
    if (0 < s) {
      requ[s] = 0;
        /* print out every combinations of requested tabled
         * 3++ letter abbreviations are matched */
        if (0 != strstr(requ, "/ccn")) send_what = SIW_CCN;
		if (0 != strstr(requ, "/reg")) send_what = SIW_CCN | ccn_register((char *)&requ);
    }
	if ( send_what == 0 ) send_what = SIW_CCN;
  }
  send_info(send_what, ipc_connection);
}

int ccn_register (char *req)
{
		char *currentoken;
		bool unregister;

		olsr_printf(3, "CCN request: %s\n", req);
		
		currentoken = strtok(req, "/");
		olsr_printf(9, "CCN currentoken0: %s\n", currentoken);
		if(currentoken == NULL)
				return SIW_CCN_REG_KO;

		currentoken = strtok(NULL, "/");
		olsr_printf(9, "CCN currentoken1: %s\n", currentoken);
		if(strcmp(currentoken, "reg"))
				return SIW_CCN_REG_KO;
		
		currentoken = strtok(NULL, "/");
		olsr_printf(9, "CCN currentoken2: %s\n", currentoken);
		if(!strcmp(currentoken, "add"))
				unregister = false;
		else if(!strcmp(currentoken, "rem") || !strcmp(currentoken, "del"))
				unregister = true;
		else
				return SIW_CCN_REG_KO;

		/* get the name to (un)register */
		currentoken = strtok(NULL, " ");
		if(!ccn_name_is_valid(currentoken))
				return SIW_CCN_REG_KO;
		olsr_printf(9, "CCN currentoken3: %s\n", currentoken);
		if(currentoken == NULL)
				return SIW_CCN_REG_KO;

		if(unregister)
		{
				olsr_printf(2, "CCN UNREGISTER ");
				olsr_printf(2, "%s\n", currentoken);
				if(ccn_name_register(currentoken, true))
						return SIW_CCN_UNREG_OK;
				else
						return SIW_CCN_UNREG_KO;
		}
		else
		{
				olsr_printf(2, "CCN REGISTER ");
				olsr_printf(2, "%s\n", currentoken);
				if(ccn_name_register(currentoken, false))
						return SIW_CCN_REG_OK;
				else
						return SIW_CCN_REG_KO;
		}
}

static void
ipc_print_ccn(struct autobuf *abuf)
{
  struct ccn_entry *current;
  struct name_entry *currentname;
  #define MAXSTRINGADDRSIZE 128
  char originatorstring[MAXSTRINGADDRSIZE];

  olsr_printf(2, "CCN get_all_names\n");

  abuf_puts(abuf, "Name\tOriginator\tLocalOriginator\n");

  if(ccn_all_entries == NULL)
		  return;

  current = ccn_all_entries->next;
  while (current != NULL)
  {
		currentname = current->names.next;
		while(currentname != NULL) 
		{
				abuf_appendf(abuf, "%s\t%s\t%s\n",
                                currentname->name,
                                inet_ntop(AF_INET, &current->originator, (char *)&originatorstring, MAXSTRINGADDRSIZE),
                                memcmp(&current->originator, &olsr_cnf->main_addr, olsr_cnf->ipsize) == 0 ? "Y" : "N");
				currentname = currentname->next;
		}
		current = current->next;
  }

  abuf_puts(abuf, "\n");
}

static void
ccninfo_write_data(void *foo __attribute__ ((unused))) {
  fd_set set;
  int result, i, j, max;
  struct timeval tv;

  FD_ZERO(&set);
  max = 0;
  for (i=0; i<outbuffer_count; i++) {
    /* And we cast here since we get a warning on Win32 */
    FD_SET((unsigned int)(outbuffer_socket[i]), &set);

    if (outbuffer_socket[i] > max) {
      max = outbuffer_socket[i];
    }
  }

  tv.tv_sec = 0;
  tv.tv_usec = 0;

  result = select(max + 1, NULL, &set, NULL, &tv);
  if (result <= 0) {
    return;
  }

  for (i=0; i<outbuffer_count; i++) {
    if (FD_ISSET(outbuffer_socket[i], &set)) {
      result = send(outbuffer_socket[i], outbuffer[i] + outbuffer_written[i], outbuffer_size[i] - outbuffer_written[i], 0);
      if (result > 0) {
        outbuffer_written[i] += result;
      }

      if (result <= 0 || outbuffer_written[i] == outbuffer_size[i]) {
        /* close this socket and cleanup*/
        close(outbuffer_socket[i]);
        free (outbuffer[i]);

        for (j=i+1; j<outbuffer_count; j++) {
          outbuffer[j-1] = outbuffer[j];
          outbuffer_size[j-1] = outbuffer_size[j];
          outbuffer_socket[j-1] = outbuffer_socket[j];
          outbuffer_written[j-1] = outbuffer_written[j];
        }
        outbuffer_count--;
      }
    }
  }
  if (outbuffer_count == 0) {
    olsr_stop_timer(writetimer_entry);
  }
}

static void
ipc_register_ccn_response(struct autobuf *abuf, bool unregister, bool ok)
{
		if(unregister)
				abuf_puts(abuf, "UNREGISTRATION\n");
		else
				abuf_puts(abuf, "REGISTRATION\n");

		if(ok)
				abuf_puts(abuf, "OK\n");
		else
				abuf_puts(abuf, "ERROR\n");

		abuf_puts(abuf, "\n");
}

static void
send_info(unsigned int send_what, int the_socket)
{
  struct autobuf abuf;

  abuf_init(&abuf, CCN_PLUGIN_BUFFER_MAX_SIZE);

  /* Print minimal http header */
  abuf_puts(&abuf, "HTTP/1.0 200 OK\n");
  abuf_puts(&abuf, "Content-type: text/plain\n\n");

  /* Print tables to IPC socket */

  /* CCN */
  if ((send_what & SIW_CCN_REG_OK) == SIW_CCN_REG_OK) ipc_register_ccn_response(&abuf, false, true);
  if ((send_what & SIW_CCN_REG_KO) == SIW_CCN_REG_KO) ipc_register_ccn_response(&abuf, false, false);
  if ((send_what & SIW_CCN_UNREG_OK) == SIW_CCN_UNREG_OK) ipc_register_ccn_response(&abuf, true, true);
  if ((send_what & SIW_CCN_UNREG_KO) == SIW_CCN_UNREG_KO) ipc_register_ccn_response(&abuf, true, false);
  if ((send_what & SIW_CCN) == SIW_CCN) ipc_print_ccn(&abuf);

  outbuffer[outbuffer_count] = olsr_malloc(abuf.len, "txt output buffer");
  outbuffer_size[outbuffer_count] = abuf.len;
  outbuffer_written[outbuffer_count] = 0;
  outbuffer_socket[outbuffer_count] = the_socket;

  memcpy(outbuffer[outbuffer_count], abuf.buf, abuf.len);
  outbuffer_count++;

  if (outbuffer_count == 1) {
    writetimer_entry = olsr_start_timer(100, 0, OLSR_TIMER_PERIODIC, &ccninfo_write_data, NULL, 0);
  }

  abuf_free(&abuf);
}

int
ccninfo_init(void)
{

		if (olsr_cnf->ip_version != AF_INET) {
				olsr_printf(0, "For the moment only IPv4 is supported. Quitting\n");
				return -1;
		}

		/* names validation regular expression */
		if(regcomp(&regx, CCN_VALID_NAMES_REGEX, REG_NOSUB) != 0)
		{
				olsr_printf(0, "CCN: error in regular expression compilation: %s\n", CCN_VALID_NAMES_REGEX);
				return false;
		}

		/* CCN message parser */
		olsr_parser_add_function(&ccn_parser, CCN_MESSAGE_TYPE);

		/* CCN message generator*/
		msg_gen_timer = olsr_start_timer(ccn_emission_interval * MSEC_PER_SEC, 0, OLSR_TIMER_PERIODIC, &ccn_generator, NULL, 0);


		return 1;
}

void
ccn_generator (void *foo __attribute__((unused)))
{
		struct ccn_message ccnmessage;
		struct ccn_message *message = (struct ccn_message *)&ccnmessage;
		struct interface *ifn;
		int currentmsgsize = 0, numberoftlvs = 0;

		print_all_ccn_entries(7);

		memset(message, 0, sizeof(struct ccn_message));

		message->olsr_msgtype = CCN_MESSAGE_TYPE;
		message->olsr_vtime = reltime_to_me(ccn_validity_time * MSEC_PER_SEC);
		memcpy(&message->originator, &olsr_cnf->main_addr, olsr_cnf->ipsize);
		message->ttl = MAX_TTL;
		message->hopcnt = 0;
		message->seqno = htons(get_msg_seqno());
		message->version = CCN_PLUGIN_VERSION;
		message->reserved = 0x0000;
		numberoftlvs = get_our_names(message->tlvs, &currentmsgsize, ifnet->netbuf.maxsize - ifnet->netbuf.pending - 32);
		if(numberoftlvs == 0)
				return;
		
		/* olsr_msg_header + ccn_header */
		currentmsgsize += 12 + 4;
		/* align message size to 32 bits */
		if (currentmsgsize % 4 != 0)
				currentmsgsize += 4 - (currentmsgsize % 4);
		message->olsr_msgsize = htons(currentmsgsize); 
		message->tlv_count = numberoftlvs;

		/* push the message */
		for (ifn = ifnet; ifn; ifn = ifn->int_next) {
		  OLSR_PRINTF(3, "CCN PLUGIN: Generating message - [%s]\n", ifn->int_name);

		  if (net_outbuffer_push(ifn, message, currentmsgsize) != currentmsgsize) {
		    /* send data and try again */
		    net_output(ifn);
		    if (net_outbuffer_push(ifn, message, currentmsgsize) != currentmsgsize) {
		  	OLSR_PRINTF(1, "CCN PLUGIN: could not send on interface: %s\n", ifn->int_name);
		    }
		  }
		}
}

bool
ccn_parser(union olsr_message *m, struct interface *in_if __attribute__ ((unused)), union olsr_ip_addr *ipaddr __attribute__((unused)))
{
		/* parse incoming olsr message */
		struct ccn_message *message;
		union olsr_ip_addr originator;
		olsr_reltime vtime;

		olsr_printf(9, "CCN PARSING\n");
		memcpy(&originator, &m->v4.originator, olsr_cnf->ipsize);

		message = (struct ccn_message *) m;

		if(message->version != CCN_PLUGIN_VERSION)
		{
				olsr_printf(1, "CCN PLUGIN WRONG VERSION! %d vs %d\n", message->version, CCN_PLUGIN_VERSION);
				return false;
		}

		vtime = me_to_reltime(message->olsr_vtime);
		if(vtime == 0)
		{
				olsr_printf(2, "CCN PLUGIN Invalid validity time! %u \n", vtime);
				return false;
		}

		print_all_ccn_entries(7);

		olsr_printf(3, "CCN: receiving %d names\n", message->tlv_count);

		return names_parser(message->tlvs, message->tlv_count, originator.v4.s_addr, vtime);
}

struct name_entry * get_name_entry(char *name, struct name_entry_head *nhead)
{
		struct name_entry *current;

		if(nhead == NULL)
				return NULL;
		
		current = nhead->next;
		while (current != NULL)
		{
				if(!strcmp(name, current->name))
						return current;

				current = current->next;
		}

		return NULL;
}

void free_name_entry(struct name_entry *nentry)
{
		free(nentry->name);
		free(nentry);
}

bool delete_name_entry(char *name, struct name_entry_head *nhead)
{ /* remove name_entry by name */
		struct name_entry *current, *previous, *nnext;

		if(nhead == NULL)
				return false;
		
		current = nhead->next;
		previous = NULL;
		while (current != NULL)
		{
				if(!strcmp(name, current->name))
				{
						nnext = current->next;
						if(previous != NULL)
								previous->next = nnext;
						else
								nhead->next = nnext;
						current->name_timer = NULL;
						free_name_entry(current);
						return true;
				}
				previous = current;
				current = current->next;
		}
		return false;
}

static void
ccn_expire_name_timer(void *context)
{
		struct name_entry *current;

		current = (struct name_entry *) context;
		if(!delete_name_entry(current->name, current->head))
				olsr_printf(1, "CCN: ERROR: could not delete expired name\n");
}

bool append_name_entry(char *name, struct name_entry_head *nhead, olsr_reltime vtime)
{
		struct name_entry *current, *previous, *new_name_entry;

		olsr_printf(4, "CCN new name %s\n", name);

		if(nhead == NULL)
				return false;

		current = get_name_entry(name, nhead);
		if(current != NULL)
		{
				/* refresh the timer */
				current->vtime = vtime;
				if(vtime > 0)
						olsr_set_timer(&(current->name_timer), vtime, 0, OLSR_TIMER_ONESHOT, &ccn_expire_name_timer, current, 0);
				else
						current->name_timer = NULL;
				return true;
		}

		new_name_entry = (struct name_entry *)malloc(sizeof(struct name_entry));
		new_name_entry->name = (char *)malloc(strlen(name) + 2);
		new_name_entry->vtime = vtime;
		strncpy(new_name_entry->name, name, strlen(name) + 1);
		new_name_entry->next = NULL;
		new_name_entry->head = nhead;

		if(vtime > 0)
				new_name_entry->name_timer = olsr_start_timer(vtime, 0, OLSR_TIMER_ONESHOT, &ccn_expire_name_timer, new_name_entry, 0);
		else
				new_name_entry->name_timer = NULL;

		current = nhead->next;
		if (current == NULL) 
		{
				nhead->next = new_name_entry;
				return true;
		}

		while (current != NULL)
		{
				previous = current;
				current = current->next;
		}

		previous->next = new_name_entry;

		return true;
}

bool ccn_name_is_valid(char *name)
{
		if(regexec(&regx, name, 0, NULL, 0) == 0)
				return true;

		return false;
}

bool names_parser(uint8_t *tlvs, unsigned int tlv_count, uint32_t originator, olsr_reltime vtime)
{
		/* parse tlvs and generate a linked list */
		struct ccn_tlv *tlv;
		char name[MAX_NAME_LEN + 1];
		unsigned int i, offset;
		bool everythingok = true;

		i = 0;
		offset = 0;
		while (i < tlv_count && offset < (MAX_TLV_PAYLOAD - sizeof(struct ccn_tlv)))
		{
				olsr_printf(9, "CCN i:%d offset:%d\n", i, offset);
				tlv = (struct ccn_tlv *) &tlvs[offset];
				offset += sizeof(struct ccn_tlv) + tlv->length;
				i++;

				olsr_printf(9, "CCN TLV: |%0x|%0x|%0x|%s|\n", tlv->type, tlv->flags, tlv->length, tlv->value);

				if (tlv->type != CCN_TLV_TYPE_URI) //TODO: CCNB
						continue;
				if (tlv->flags != CCN_TLV_DEFAULT_FLAGS) //TODO: parse TLV flags
						continue;

				memcpy((char *)&name, tlv->value, tlv->length);
				name[tlv->length] = '\0';
				olsr_printf(4, "CCN validating name\n");
				if(ccn_name_is_valid((char *)&name))
						olsr_printf(4, "CCN name parsed and validated: %s\n", name);
				else
						olsr_printf(2, "Invalid CCN name received\n");

				if(!add_ccn_name(originator, name, vtime))
						everythingok = false;

		}

		return everythingok;
}

void free_name_entries(struct name_entry_head *name_head)
{
		struct name_entry *current, *next; 
		
		olsr_printf(3, "CCN free_name_entries\n");

		if (name_head == NULL)
				return;
		
		current = name_head->next;
		while(current != NULL)
		{
				next = current->next;
				olsr_printf(5, "CCN freeing %s\n", current->name);
				free_name_entry(current);
				current = next;
		}

		name_head->next = NULL;
}

bool ccn_name_register(char *name, bool unregister)
{ /* (un)register a name to be announced by ourselves */
		struct ccn_entry *current;

		/* find our cnn_entry */
		current = get_ccn_entry_by_originator(olsr_cnf->main_addr.v4.s_addr);

		if(!unregister)
		{ /* registration */
				if (current != NULL && get_name_entry(name, &current->names) != NULL)
						return true;  /* already in the list */
				return add_ccn_name(olsr_cnf->main_addr.v4.s_addr, name, 0);
		} else { /* unregistration */
				if (current == NULL || get_name_entry(name, &current->names) == NULL)
						return true;  /* already not in the list */
				return delete_name_entry(name, &current->names);
		}
}

int get_our_names(uint8_t *tlvs, int *length, int maxlength)
{ /* write tlvs for the names we are announcing inside tlvs */
		struct ccn_entry *current;
		bool found = false;
		int numberofnames = 0, offset = 0;
		struct ccn_tlv *tlv;

		memset(tlvs, 0, MAX_TLV_PAYLOAD);

		if(current_our_name == NULL && ccn_all_entries != NULL)
		{
				/* find our cnn_entry */
				current = ccn_all_entries->next;
				while (current != NULL && !found)
				{
						if (current->originator == olsr_cnf->main_addr.v4.s_addr)
								found = true;
						else
								current = current->next;
				}
				if(!found)
						return false;

				if(current == NULL)
						return false;

				current_our_name = current->names.next;

				olsr_printf(5, "CCN: name loop (re)start");
				if (current_our_name != NULL)
						olsr_printf(5, " from %s\n", current_our_name->name);
				else
						olsr_printf(5, ". No names to be announced.\n");
		}

		while(current_our_name != NULL && offset + (int)strlen(current_our_name->name) < maxlength)
		{
				tlv = (struct ccn_tlv*)(tlvs + offset);

				tlv->type = CCN_TLV_TYPE_URI; /*TODO: CCNB*/
				tlv->flags = CCN_TLV_DEFAULT_FLAGS;
				tlv->length = strlen(current_our_name->name);

				memcpy(tlv->value, current_our_name->name, strlen(current_our_name->name));

				offset += sizeof(struct ccn_tlv) + strlen(current_our_name->name);
				numberofnames++;
				current_our_name = current_our_name->next;
		}

		memcpy(length, &offset, sizeof(int));

		olsr_printf(3, "CCN message: %d names, %d bytes\n", numberofnames, offset);

		return numberofnames;
}

bool add_ccn_name(uint32_t originator, char *name, olsr_reltime vtime)
{
		struct ccn_entry *ourentry, *current;

		olsr_printf(4, "CCN add name %s\n", name);

		ourentry = get_ccn_entry_by_originator(originator);

		if(ourentry != NULL)
				return append_name_entry(name, &(ourentry->names), vtime);

		if(ccn_all_entries == NULL)
		{
				ccn_all_entries = (struct ccn_entry_head *) malloc(sizeof(struct ccn_entry_head));
				memset(ccn_all_entries, 0, sizeof(struct ccn_entry_head));
				ccn_all_entries->next = (struct ccn_entry *) malloc(sizeof(struct ccn_entry));
				memset(ccn_all_entries->next, 0, sizeof(struct ccn_entry));
				ccn_all_entries->next->originator = originator;
				ourentry = ccn_all_entries->next;
				return append_name_entry(name, &(ourentry->names), vtime);
		}

		ourentry = (struct ccn_entry *)malloc(sizeof(struct ccn_entry));
		ourentry->names.next = NULL;
		ourentry->originator = originator;
		ourentry->next = NULL;

		current = ccn_all_entries->next;
		while(current->next != NULL)
				current = current->next;

		current->next = ourentry;

		return append_name_entry(name, &(ourentry->names), vtime);
}

void free_ccn_entries(void) 
{
		struct ccn_entry *current, *next;

		if(ccn_all_entries == NULL)
				return;
		current = ccn_all_entries->next;
		while (current != NULL)
		{
				next = current->next;
				free_name_entries(&(current->names));
				free(current);
				current = next;
		}
}

void print_all_ccn_entries(int debuglevel)
{
		struct ccn_entry *current;
		struct name_entry *currentname; 

		if(ccn_all_entries == NULL)
				return;

		current = ccn_all_entries->next;
		while(current != NULL)
		{
				olsr_printf(debuglevel, "CCN: originator: %x", current->originator);
				currentname = current->names.next;
				while(currentname != NULL)
				{
						olsr_printf(debuglevel, "[%s]", currentname->name);
						currentname = currentname->next;
				}
				current = current->next;
				olsr_printf(debuglevel, "\n");
		}
}

struct ccn_entry * get_ccn_entry_by_originator(uint32_t originator)
{
  struct ccn_entry *current;

  if(ccn_all_entries == NULL)
		  return NULL;
  if(ccn_all_entries->next == NULL)
		  return NULL;

  current = ccn_all_entries->next;
  while (current != NULL)
  {
		if (current->originator == originator)
			return current;
		current = current->next;
  }

  return NULL;
}


/*
 * Local Variables:
 * mode: c
 * style: linux
 * c-basic-offset: 4
 * indent-tabs-mode: nil
 * End:
 */
